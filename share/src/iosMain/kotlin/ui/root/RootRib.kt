package ui.root

import mvi.Storage
import ribs.*
import ui.login.LoginBuilder
import platform.UIKit.UIViewController
import platform.UIKit.addSubview
import platform.UIKit.removeFromSuperview
import platform.UIKit.setFrame
import utils.ui.Screen

actual class RootView : RenderView<Any,Any> {
    public val controller = UIViewController(null, null)
    /*
    private var currentView: RibView? = null

    override fun show(newView: RibView) {
        currentView?.removeFromSuperview()
        controller.view.addSubview(newView)

        newView.setFrame(controller.view.bounds)
        currentView = newView
    }*/
    override fun render(state: Any) {
    }

    override fun setupPresenter(presenter: Storage<Any, Any, out Any>) {
    }

    override fun getScreen(): Screen<out Any, out Any> {
        return Screen(this,Screen.ScreenType.Root)
    }
}

actual class RootViewProvider actual constructor(private val dependencies: OSSpecificDependencies) {
    actual fun getView() = RootView()
}

actual interface OSSpecificDependencies: ViewCreator,ViewNavigator